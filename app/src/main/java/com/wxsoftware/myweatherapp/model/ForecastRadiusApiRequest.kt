package com.wxsoftware.myweatherapp.model

import com.wxsoftware.myweatherapp.model.forecast_radius.City


data class ForecastRadiusApiRequest(
    val cod: String,
    val count: Int,
    val list: List<City>,
    val message: String
)