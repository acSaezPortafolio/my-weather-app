package com.wxsoftware.myweatherapp.model.forecast_radius

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Sys(
    val country: String
): Parcelable