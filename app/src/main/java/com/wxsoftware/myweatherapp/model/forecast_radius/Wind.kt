package com.wxsoftware.myweatherapp.model.forecast_radius

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Wind(
    val deg: Int,
    val gust: Int,
    val speed: Double
): Parcelable