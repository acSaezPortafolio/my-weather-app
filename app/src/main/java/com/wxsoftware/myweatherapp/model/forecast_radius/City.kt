package com.wxsoftware.myweatherapp.model.forecast_radius

import android.os.Parcelable
import androidx.room.*
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class City(
    @PrimaryKey
    var id: Int = 0,
    @Embedded
    var clouds: Clouds = Clouds(0),
    @Embedded
    var coord: Coord = Coord(0.0, 0.0),
    var dt: Int = 0,
    @Embedded
    var main: Main = Main(0,0,0.0,0.0,0.0),
    var name: String = "",
    @Embedded
    var sys: Sys = Sys(""),
    @Ignore
    var weather: List<Weather> = arrayListOf(),
    @Embedded
    var wind: Wind = Wind(0,0,0.0),
    var favourite: Boolean = false
): Parcelable