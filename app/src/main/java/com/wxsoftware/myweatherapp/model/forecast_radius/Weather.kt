package com.wxsoftware.myweatherapp.model.forecast_radius

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(
    foreignKeys = [
        ForeignKey(
            entity = City::class,
            parentColumns = ["id"],
            childColumns = ["cityId"],
            onDelete = ForeignKey.CASCADE
            )
    ]
)
@Parcelize
data class Weather(
    var description: String = "",
    var icon: String = "",
    @PrimaryKey(autoGenerate = true)
    var id: Int? = 0,
    var main: String = "",
    @ColumnInfo(index = true)
    var cityId: Int = 0
): Parcelable