package com.wxsoftware.myweatherapp.extensions

import com.wxsoftware.myweatherapp.model.forecast_radius.Wind as RoomWind
import com.wxsoftware.domain.forecast_radius.Wind as DomainWind
import com.wxsoftware.myweatherapp.model.forecast_radius.Sys as RoomSys
import com.wxsoftware.domain.forecast_radius.Sys as DomainSys
import com.wxsoftware.myweatherapp.model.forecast_radius.Main as RoomMain
import com.wxsoftware.domain.forecast_radius.Main as DomainMain
import com.wxsoftware.myweatherapp.model.forecast_radius.Coord as RoomCoord
import com.wxsoftware.domain.forecast_radius.Coord as DomainCoord
import com.wxsoftware.domain.forecast_radius.City as DomainCity
import com.wxsoftware.myweatherapp.model.forecast_radius.City as RoomCity
import com.wxsoftware.domain.forecast_radius.Clouds as DomainClouds
import com.wxsoftware.myweatherapp.model.forecast_radius.Clouds as RoomClouds
import com.wxsoftware.domain.forecast_radius.Weather as DomainWeather
import com.wxsoftware.myweatherapp.model.forecast_radius.Weather as RoomWeather

fun DomainCity.toRoomCity(): RoomCity =
    RoomCity(
        id, clouds.toRoomClouds(), coord.toRoomCoord(), dt, main.toRoomMain(),
        name, sys.toRoomSys(), weather.map(DomainWeather::toRoomWeather),
        wind.toRoomWind(), favourite
    )

fun RoomCity.toDomainCity(): DomainCity = DomainCity(
    id, clouds.toDomainClouds(), coord.toDomainCoord(), dt, main.toDomainMain(),
    name, sys.toDomainSys(), weather.map(RoomWeather::toDomainWeather),
    wind.toDomainWind(), favourite
)

fun DomainWeather.toRoomWeather(): RoomWeather = RoomWeather (
        description, icon, id, main, cityId
    )

fun RoomWeather.toDomainWeather(): DomainWeather = DomainWeather(
    description, icon, id, main, cityId
)

fun DomainClouds.toRoomClouds(): RoomClouds = RoomClouds(all)

fun RoomClouds.toDomainClouds(): DomainClouds = DomainClouds(all)

fun DomainCoord.toRoomCoord(): RoomCoord = RoomCoord(lat, lon)

fun RoomCoord.toDomainCoord(): DomainCoord = DomainCoord(lat, lon)

fun DomainMain.toRoomMain(): RoomMain = RoomMain(humidity, pressure, temp, temp_max, temp_min)

fun RoomMain.toDomainMain(): DomainMain = DomainMain(humidity, pressure, temp, temp_max, temp_min)

fun DomainSys.toRoomSys(): RoomSys = RoomSys(country)

fun RoomSys.toDomainSys(): DomainSys = DomainSys(country)

fun DomainWind.toRoomWind(): RoomWind = RoomWind(deg, gust, speed)

fun RoomWind.toDomainWind(): DomainWind = DomainWind(deg, gust, speed)