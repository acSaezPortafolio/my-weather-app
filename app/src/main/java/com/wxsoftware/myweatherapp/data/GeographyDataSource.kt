package com.wxsoftware.myweatherapp.data

import android.app.Application
import android.content.Context
import android.location.Location
import android.location.LocationManager
import android.util.Log
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.wxsoftware.data.datasource.LocationDataSource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume

class GeographyDataSource(private val application: Application) : LocationDataSource {

    private val fusedLocationClient: FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(application)

    override suspend fun isGpsEnabled(): Boolean {
        val locationManager: LocationManager = application.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    @ExperimentalCoroutinesApi
    override suspend fun lastLocationSuspended(): Pair<Double, Double>? =
        suspendCancellableCoroutine { cont ->
            fusedLocationClient.lastLocation.addOnCompleteListener { task ->
                val location: Location? = task.result
                if (location != null) {
                    Log.d("LOCATION", "latitude: ${location.latitude} --- longitude: ${location.longitude}")
                    cont.resume(Pair(location.latitude, location.longitude))
                } else {
                    cont.resume(null)
                }
            }
        }
}