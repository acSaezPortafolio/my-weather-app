package com.wxsoftware.myweatherapp.data.database

import androidx.room.*
import com.wxsoftware.myweatherapp.model.forecast_radius.City
import com.wxsoftware.myweatherapp.model.forecast_radius.Weather

@Dao
interface CityDao {

    @Query("SELECT COUNT(id) FROM City")
    fun cityCount(): Int

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertCity(city: City)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertCities(cities: List<City>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertWeather(weather: Weather): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateCity(city: City)

    @Query("SELECT * FROM City")
    fun getAll(): List<City>

    @Query("SELECT * FROM City WHERE id = :cityId")
    fun getCityById(cityId: Int): City

    @Query("SELECT * FROM Weather WHERE cityId = :cityId")
    fun getWeathers(cityId: Int): List<Weather>

    @Query("DELETE FROM City")
    fun nukeCity()

    @Query("DELETE FROM Weather")
    fun nukeWeather()
}