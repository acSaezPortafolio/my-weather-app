package com.wxsoftware.myweatherapp.data.database

import com.wxsoftware.data.datasource.LocalDataSource
import com.wxsoftware.myweatherapp.extensions.toDomainCity
import com.wxsoftware.myweatherapp.extensions.toRoomCity
import com.wxsoftware.myweatherapp.extensions.toRoomWeather
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import com.wxsoftware.myweatherapp.model.forecast_radius.City as RoomCity
import com.wxsoftware.domain.forecast_radius.City as DomainCity

class RoomDataSource(private val db: CityDatabase) : LocalDataSource {

    private val cityDao = db.cityDao()

    override suspend fun isEmpty(): Boolean = withContext(Dispatchers.IO) {
        cityDao.cityCount() == 0
    }

    override suspend fun saveCities(cities: List<DomainCity>) = withContext(Dispatchers.IO) {
        with(cityDao) {
            insertCities(cities.map(DomainCity::toRoomCity))
            cities.forEach { city ->
                city.weather.forEach { weather ->
                    weather.cityId = city.id
                    //all weathers have the same id
                    weather.id = null
                    insertWeather(weather.toRoomWeather())
                }
            }
        }
    }

    override suspend fun getCitiesAround(): List<DomainCity> = withContext(Dispatchers.IO) {
        val citiesList = cityDao.getAll()
        citiesList.forEach { city ->
            city.weather = cityDao.getWeathers(city.id)
        }
        citiesList.map(RoomCity::toDomainCity)
    }

    override suspend fun findCityById(cityId: Int): DomainCity = withContext(Dispatchers.IO) {
        val city = db.cityDao().getCityById(cityId)
        city.weather = db.cityDao().getWeathers(city.id)
        city.toDomainCity()
    }

    override suspend fun nukeData() = withContext(Dispatchers.IO) {
        db.cityDao().nukeCity()
    }

    override suspend fun update(city: DomainCity) = withContext(Dispatchers.IO) {
        db.cityDao().updateCity(city.toRoomCity())
    }
}