package com.wxsoftware.myweatherapp.data

import android.app.Application
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import com.wxsoftware.data.repository.PermissionChecker

class AndroidPermissionChecker(private val application: Application): PermissionChecker {

    override suspend fun request(permissions: List<String>): Boolean {
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(application, permission) != PackageManager.PERMISSION_GRANTED)
                return false
        }
        return true
    }
}