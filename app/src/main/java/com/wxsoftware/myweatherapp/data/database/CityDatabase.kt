package com.wxsoftware.myweatherapp.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.wxsoftware.myweatherapp.model.forecast_radius.City
import com.wxsoftware.myweatherapp.model.forecast_radius.Weather

@Database(
    entities = [City::class, Weather::class],
    version = 1
)
abstract class CityDatabase: RoomDatabase() {

    companion object {
        fun build(context: Context): CityDatabase = Room.databaseBuilder(
            context,
            CityDatabase::class.java,
            "city-db"
        ).build()
    }

    abstract fun cityDao(): CityDao
}