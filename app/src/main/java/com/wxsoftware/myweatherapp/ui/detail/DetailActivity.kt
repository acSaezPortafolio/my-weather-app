package com.wxsoftware.myweatherapp.ui.detail

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.wxsoftware.myweatherapp.R
import kotlinx.android.synthetic.main.activity_detail.*
import org.koin.androidx.scope.currentScope
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DetailActivity : AppCompatActivity() {

    companion object {
        const val CITY = "city"
    }

    private val viewModel: DetailViewModel by currentScope.viewModel(this) {
        parametersOf(intent.getIntExtra(CITY, 0))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        favouriteButton.setOnClickListener { viewModel.onFavouriteClick() }

        viewModel.model.observe(this, Observer(::showData))
    }

    @SuppressLint("SetTextI18n")
    private fun showData(model: DetailViewModel.UiModel) = with(model.city) {

        Glide
            .with(applicationContext)
            .load(applicationContext.getString(R.string.weatherIcon_url, this.weather[0].icon))
            .fitCenter()
            .into(backgroundDetail)

        city.text = name
        temperature.text = "${main.temp.toInt()} ºC"
        weatherText.text = weather[0].description
        humidity.text = "Humedad: ${main.humidity}"
        pressure.text = "Presión: ${main.pressure}"
        temp_max.text = "Temperatura máxima: ${main.temp_max} ºC"
        temp_min.text = "Temperatura mínima: ${main.temp_min} ºC"
        wind_speed.text = "Velocidad del viento: ${wind.speed}"

        val favouriteIcon = if (favourite) R.drawable.ic_favourite_on
                        else R.drawable.ic_favourite_off
        favouriteButton.setImageDrawable(getDrawable(favouriteIcon))
    }
}
