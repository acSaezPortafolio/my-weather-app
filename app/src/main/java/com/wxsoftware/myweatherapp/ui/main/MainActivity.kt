package com.wxsoftware.myweatherapp.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import com.wxsoftware.domain.forecast_radius.City
import com.wxsoftware.myweatherapp.R
import com.wxsoftware.myweatherapp.extensions.hide
import com.wxsoftware.myweatherapp.extensions.show
import com.wxsoftware.myweatherapp.ui.commons.ForecastApp
import com.wxsoftware.myweatherapp.ui.commons.PermissionRequester
import com.wxsoftware.myweatherapp.ui.detail.DetailActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.scope.currentScope
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by currentScope.viewModel(this)

    private val permissionRequester = PermissionRequester(this)

    private lateinit var adapter: CityListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = CityListAdapter(viewModel::onCityClicked)
        citiesList.addItemDecoration(DividerItemDecoration(citiesList.context,
            DividerItemDecoration.VERTICAL))
        citiesList.adapter = adapter
        viewModel.model.observe(this, Observer(::updateUi))
    }

    private fun updateUi(model: MainViewModel.UiModel) {
        if (model == MainViewModel.UiModel.Loading) chargingWeatherProgressBar1.show()
        else chargingWeatherProgressBar1.hide()

        when (model) {
            is MainViewModel.UiModel.Content -> {
                adapter.cities = model.citiesList
                adapter.notifyDataSetChanged()
            }
            is MainViewModel.UiModel.Navigation -> navigateToDetail(model.city)
            is MainViewModel.UiModel.RequestPermission -> permissionRequester.request() {
                viewModel.onPermissionRequested()
            }
        }
    }


    private fun navigateToDetail(city: City) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(DetailActivity.CITY, city.id)
        startActivity(intent)
    }
}
