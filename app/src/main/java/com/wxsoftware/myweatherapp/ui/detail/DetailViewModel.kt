package com.wxsoftware.myweatherapp.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.wxsoftware.domain.forecast_radius.City
import com.wxsoftware.myweatherapp.ui.commons.ScopedViewModel
import com.wxsoftware.usecases.FindCityById
import com.wxsoftware.usecases.ToggleCityFavourite
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch

class DetailViewModel(
    private val cityId: Int,
    private val findCityById: FindCityById,
    private val toggleCityFavourite: ToggleCityFavourite,
    uiDispatcher: CoroutineDispatcher
): ScopedViewModel(uiDispatcher) {

    data class UiModel(val city: City)

    private val _model = MutableLiveData<UiModel>()
    val model: LiveData<UiModel>
        get() {
            if (_model.value == null) findCity()
            return _model
        }


    private fun findCity() =
        launch {
            _model.value = UiModel(findCityById.invoke(cityId))
        }


    fun onFavouriteClick() = launch {
        _model.value?.city?.let { c ->
            toggleCityFavourite.invoke(c)
            _model.value = UiModel(c)
        }
    }
}