package com.wxsoftware.myweatherapp.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.wxsoftware.domain.forecast_radius.City
import com.wxsoftware.myweatherapp.ui.commons.ScopedViewModel
import com.wxsoftware.usecases.GetAroundLocation
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch

class MainViewModel(
    private val getAroundLocation: GetAroundLocation,
    uiDispatcher: CoroutineDispatcher
    ):
     ScopedViewModel(uiDispatcher) {


    sealed class UiModel {
        object Loading: UiModel()
        data class Content(val citiesList: List<City>): UiModel()
        data class Navigation(val city: City): UiModel()
        object RequestPermission : UiModel()
    }

    private val _model = MutableLiveData<UiModel>()
    val model: LiveData<UiModel>
        get() {
            //to call refresh for first time
            if (_model.value == null) refresh()
            return _model
        }


    private lateinit var citiesList: List<City>

    private  fun refresh() {
        _model.value = UiModel.RequestPermission
    }


    fun onPermissionRequested() {
        launch {
            _model.value = UiModel.Loading
            citiesList = getAroundLocation.invoke()
            _model.value = UiModel.Content(citiesList)
        }
    }

    fun onCityClicked(city: City) {
        _model.value = UiModel.Navigation(city)
    }
}