package com.wxsoftware.myweatherapp.ui.main

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.wxsoftware.domain.forecast_radius.City
import com.wxsoftware.myweatherapp.R
import com.wxsoftware.myweatherapp.extensions.inflate
import kotlinx.android.synthetic.main.forecast_list_row.view.*

class CityListAdapter(private var listener: (City) -> Unit):
    RecyclerView.Adapter<CityListAdapter.ViewHolder>() {

    var cities = listOf<City>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       val view = parent.inflate(R.layout.forecast_list_row, false)
       return ViewHolder(view)
    }

    override fun getItemCount(): Int = cities.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val city = cities[position]
        holder.bindData(city)
        holder.itemView.setOnClickListener { listener(city) }
    }


    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        fun bindData(city: City) {
//            itemView.cityDegree.text = "${city.main.temp.toInt()}ºC"
            itemView.cityName.text = city.name
            itemView.cityWeather.text = city.weather[0].main

            Glide
                .with(itemView)
                .load(itemView.context.getString(R.string.weatherIcon_url, city.weather[0].icon))
                .centerCrop()
                .into(itemView.weatherIcon)
        }
    }

}