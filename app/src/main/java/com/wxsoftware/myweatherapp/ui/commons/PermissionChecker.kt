package com.wxsoftware.myweatherapp.ui.commons

//import android.Manifest
//import android.app.Activity
//import android.app.Application
//import android.content.pm.PackageManager
//import androidx.core.content.ContextCompat
//import com.karumi.dexter.Dexter
//import com.karumi.dexter.MultiplePermissionsReport
//import com.karumi.dexter.PermissionToken
//import com.karumi.dexter.listener.PermissionRequest
//import com.karumi.dexter.listener.multi.MultiplePermissionsListener
//import kotlinx.coroutines.suspendCancellableCoroutine
//import java.security.Permission
//import kotlin.coroutines.resume
//
//class PermissionChecker(private val application: Application, private vararg val permissions: String) {
//
//    fun request(): Boolean {
//        for (permission in permissions) {
//            if (ContextCompat.checkSelfPermission(application, permission) != PackageManager.PERMISSION_GRANTED)
//                return false
//        }
//        return true
//    }
//}