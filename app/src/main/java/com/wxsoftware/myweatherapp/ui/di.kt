package com.wxsoftware.myweatherapp.ui

import android.Manifest
import android.app.Application
import com.wxsoftware.data.repository.PermissionChecker
import com.wxsoftware.data.datasource.LocalDataSource
import com.wxsoftware.data.datasource.LocationDataSource
import com.wxsoftware.data.datasource.RemoteDataSource
import com.wxsoftware.data.repository.ForecastRepository
import com.wxsoftware.data.repository.LocationRepository
import com.wxsoftware.myweatherapp.R
import com.wxsoftware.myweatherapp.data.AndroidPermissionChecker
import com.wxsoftware.myweatherapp.data.GeographyDataSource
import com.wxsoftware.myweatherapp.data.database.CityDatabase
import com.wxsoftware.myweatherapp.data.database.RoomDataSource
import com.wxsoftware.myweatherapp.service.apiweather.OpenWeather
import com.wxsoftware.myweatherapp.service.apiweather.TheOpenWeatherDataSource
import com.wxsoftware.myweatherapp.ui.detail.DetailActivity
import com.wxsoftware.myweatherapp.ui.detail.DetailViewModel
import com.wxsoftware.myweatherapp.ui.main.MainActivity
import com.wxsoftware.myweatherapp.ui.main.MainViewModel
import com.wxsoftware.usecases.FindCityById
import com.wxsoftware.usecases.GetAroundLocation
import com.wxsoftware.usecases.ToggleCityFavourite
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.qualifier.named
import org.koin.dsl.module

fun Application.initDI() {
    startKoin {
        androidLogger()
        androidContext(this@initDI)
        modules(listOf(appModule, dataModule, scopesModule))
    }
}


private val appModule = module {
    single(named("apiKey")) { androidApplication().getString(R.string.apiKey) }
    single { CityDatabase.build(get()) }
    factory<LocalDataSource> { RoomDataSource(get()) }
    factory<RemoteDataSource> { TheOpenWeatherDataSource(get()) }
    factory<LocationDataSource> { GeographyDataSource(get()) }
    factory<PermissionChecker> { AndroidPermissionChecker(get()) }
    single<CoroutineDispatcher> { Dispatchers.Main }
    factory(named("accessCoarseLocationPermission")) { Manifest.permission.ACCESS_COARSE_LOCATION }
    factory(named("fineLocationPermission")) { Manifest.permission.ACCESS_FINE_LOCATION }
    factory(named("internetPermission")){ Manifest.permission.INTERNET }
    single(named("baseUrl")) { androidApplication().getString(R.string.base_url) }
    single { OpenWeather(get(named("baseUrl"))) }
}

val dataModule = module {
    factory { ForecastRepository(get(), get(), get(named("apiKey")), get()) }
    factory { LocationRepository(
        get(),
        get(),
        get(named("accessCoarseLocationPermission")),
        get(named("fineLocationPermission")),
        get(named("internetPermission"))
        ) }
}

private val scopesModule = module {
    scope(named<MainActivity>()) {
        viewModel { MainViewModel(get(), get()) }
        scoped { GetAroundLocation(get()) }
    }

    scope(named<DetailActivity>()) {
        viewModel { (id: Int) -> DetailViewModel(id, get(), get(), get()) }
        scoped { FindCityById(get()) }
        scoped { ToggleCityFavourite(get()) }
    }
}