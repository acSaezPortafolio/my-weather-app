package com.wxsoftware.myweatherapp.ui.commons

import android.app.Application
import com.wxsoftware.data.datasource.LocalDataSource
import com.wxsoftware.myweatherapp.data.database.RoomDataSource
import com.wxsoftware.myweatherapp.ui.initDI
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class ForecastApp: Application() {

    private val roomDataSource: LocalDataSource by inject()


    override fun onCreate() {
        super.onCreate()

        initDI()

        CoroutineScope(Dispatchers.IO).launch {
           roomDataSource.nukeData()
        }
    }
}