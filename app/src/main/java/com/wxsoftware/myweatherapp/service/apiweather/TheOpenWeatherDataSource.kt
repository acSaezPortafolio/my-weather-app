package com.wxsoftware.myweatherapp.service.apiweather

import com.wxsoftware.data.datasource.RemoteDataSource
import com.wxsoftware.myweatherapp.extensions.toDomainCity
import com.wxsoftware.myweatherapp.extensions.toRoomCity
import com.wxsoftware.myweatherapp.model.forecast_radius.City as RoomCity
import com.wxsoftware.domain.forecast_radius.City as DomainCity
import com.wxsoftware.myweatherapp.model.ForecastRadiusApiRequest as RoomForecastRadiusApiRequest
import com.wxsoftware.domain.ForecastRadiusApiRequest as DomainForecastRadiusApiRequest
import retrofit2.await

class TheOpenWeatherDataSource(
    private val openWeather: OpenWeather
): RemoteDataSource {



    override suspend fun getForecastRadiusApiRequest(apiKey: String, lat: Double, long: Double): DomainForecastRadiusApiRequest =
        openWeather.service
            .getCitiesByLocation(apiKey, lat, long).await().toDomain()
}

fun DomainForecastRadiusApiRequest.toRoom(): RoomForecastRadiusApiRequest = RoomForecastRadiusApiRequest(
    cod, count, list.map(DomainCity::toRoomCity), message
)

fun RoomForecastRadiusApiRequest.toDomain(): DomainForecastRadiusApiRequest = DomainForecastRadiusApiRequest(
    cod, count, list.map(RoomCity::toDomainCity), message
)