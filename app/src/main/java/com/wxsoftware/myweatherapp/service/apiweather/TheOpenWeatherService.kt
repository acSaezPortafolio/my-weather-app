package com.wxsoftware.myweatherapp.service.apiweather

import com.wxsoftware.myweatherapp.model.ForecastRadiusApiRequest
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface TheOpenWeatherService {

    @GET("data/2.5/find?units=metric&lang=es&cont=50")
    fun getCitiesByLocation(@Query("appid") appId: String,
                            @Query("lat") lat: Double?,
                            @Query("lon") long: Double?
    ): Call<ForecastRadiusApiRequest>
}