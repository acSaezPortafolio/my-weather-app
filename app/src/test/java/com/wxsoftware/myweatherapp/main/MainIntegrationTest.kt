package com.wxsoftware.myweatherapp.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.verify
import com.wxsoftware.data.datasource.LocalDataSource
import com.wxsoftware.myweatherapp.FakeLocalDataSource
import com.wxsoftware.myweatherapp.cityMocked
import com.wxsoftware.myweatherapp.defaultFakeCities
import com.wxsoftware.myweatherapp.initMockedDi
import com.wxsoftware.myweatherapp.ui.main.MainViewModel
import com.wxsoftware.usecases.GetAroundLocation
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.dsl.module
import org.koin.test.AutoCloseKoinTest
import org.koin.test.get
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class MainIntegrationTest: AutoCloseKoinTest() {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var observer: Observer<MainViewModel.UiModel>

    private lateinit var vm: MainViewModel

    @Before
    fun setUp() {
        val vmModule = module {
            factory { MainViewModel(get(), get()) }
            factory { GetAroundLocation(get()) }
        }

        initMockedDi(vmModule)
        vm = get()
    }

    @Test
    fun `data is loaded from server when local database is empty`() {
        vm.model.observeForever(observer)

        vm.onPermissionRequested()

        verify(observer).onChanged(MainViewModel.UiModel.Content(defaultFakeCities))
    }

    @Test
    fun `data is loaded from local database when data is available`() {
        val fakeLocalCities = mutableListOf(
            cityMocked.copy(20),
            cityMocked.copy(21)
        )

        val localDataSource = get<LocalDataSource>() as FakeLocalDataSource
        localDataSource.cities = fakeLocalCities
        vm.model.observeForever(observer)

        vm.onPermissionRequested()

        verify(observer).onChanged(MainViewModel.UiModel.Content(fakeLocalCities))
    }
}