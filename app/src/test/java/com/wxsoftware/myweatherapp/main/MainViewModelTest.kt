package com.wxsoftware.myweatherapp.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.wxsoftware.myweatherapp.cityMocked
import com.wxsoftware.myweatherapp.ui.main.MainViewModel
import com.wxsoftware.myweatherapp.ui.main.MainViewModel.UiModel.Content
import com.wxsoftware.usecases.GetAroundLocation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var getAroundLocation: GetAroundLocation

    @Mock
    lateinit var observer: Observer<MainViewModel.UiModel>

    private lateinit var viewModel: MainViewModel

    @Before
    fun setUp() {
        viewModel = MainViewModel(getAroundLocation, Dispatchers.Unconfined)
    }

    @Test
    fun `initializing viewModel permission are requested`() {
        viewModel.model.observeForever(observer)

        verify(observer).onChanged(MainViewModel.UiModel.RequestPermission)
    }

    @Test
    fun `after permission requested, loading is shown`() {
        runBlocking {
            val cities = listOf(cityMocked.copy(id = 1))

            whenever(getAroundLocation.invoke()).thenReturn(cities)
            viewModel.model.observeForever(observer)

            viewModel.onPermissionRequested()

            verify(observer).onChanged(MainViewModel.UiModel.Loading)
        }
    }


    @Test
    fun `after permission requested, getAroundLocation is called`() {
        runBlocking {
            val cities = listOf(cityMocked.copy(id = 2))
            whenever(getAroundLocation.invoke()).thenReturn(cities)

            viewModel.model.observeForever(observer)

            viewModel.onPermissionRequested()

            verify(observer).onChanged(Content(cities))

        }
    }
}