package com.wxsoftware.myweatherapp.detail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.verify
import com.wxsoftware.data.datasource.LocalDataSource
import com.wxsoftware.myweatherapp.FakeLocalDataSource
import com.wxsoftware.myweatherapp.cityMocked
import com.wxsoftware.myweatherapp.defaultFakeCities
import com.wxsoftware.myweatherapp.initMockedDi
import com.wxsoftware.myweatherapp.model.forecast_radius.City
import com.wxsoftware.myweatherapp.ui.detail.DetailViewModel
import com.wxsoftware.usecases.FindCityById
import com.wxsoftware.usecases.ToggleCityFavourite
import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module
import org.koin.test.AutoCloseKoinTest
import org.koin.test.get
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DetailIntegrationTest: AutoCloseKoinTest() {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var observer: Observer<DetailViewModel.UiModel>

    private lateinit var vm: DetailViewModel
    private lateinit var localDataSource: FakeLocalDataSource

    @Before
    fun setUp() {
        val vmModule = module {
            factory { (id: Int) -> DetailViewModel(id, get(), get(), get()) }
            factory { FindCityById(get()) }
            factory { ToggleCityFavourite(get()) }
        }

        initMockedDi(vmModule)
        vm = get { parametersOf(1) }
        localDataSource = get<LocalDataSource>() as FakeLocalDataSource
        localDataSource.cities = defaultFakeCities
    }

    @Test
    fun `a observing LiveData finds the city`() {
        vm.model.observeForever(observer)

        verify(observer).onChanged(DetailViewModel.UiModel(cityMocked.copy(1)))
    }

    @Test
    fun `b favourite is updated in local data base`() {
        vm.model.observeForever(observer)

//        localDataSource.cities[0].favourite = true
        vm.onFavouriteClick()

        runBlocking {
            val city = localDataSource.findCityById(1).favourite
            assertTrue(city)
        }
    }
}