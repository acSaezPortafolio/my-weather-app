package com.wxsoftware.myweatherapp.detail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.wxsoftware.myweatherapp.cityMocked
import com.wxsoftware.myweatherapp.ui.detail.DetailViewModel
import com.wxsoftware.myweatherapp.ui.main.MainViewModel
import com.wxsoftware.usecases.FindCityById
import com.wxsoftware.usecases.ToggleCityFavourite
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DetailViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var observer: Observer<DetailViewModel.UiModel>

    @Mock
    private lateinit var findCityById: FindCityById

    @Mock
    private lateinit var toggleCityFavourite: ToggleCityFavourite

    private lateinit var viewModel: DetailViewModel

    @Before
    fun setUp() {
        viewModel = DetailViewModel(1, findCityById, toggleCityFavourite, Dispatchers.Unconfined)
    }

    @Test
    fun `observing LiveData find the city`() {
        val city = cityMocked.copy(id = 1)

        runBlocking {
            whenever(findCityById.invoke(1)).thenReturn(city)
            viewModel.model.observeForever(observer)
        }

        verify(observer).onChanged(DetailViewModel.UiModel(city))
    }

    @Test
    fun `when favourite clicked, the toggleCityFavourite use case is called`() {
        runBlocking {
            val city = cityMocked.copy(1)
            whenever(findCityById.invoke(1)).thenReturn(city)
//            whenever(toggleCityFavourite.invoke(city)).thenReturn(cityMocked.copy(favourite = !city.favourite))
            viewModel.model.observeForever(observer)
            viewModel.onFavouriteClick()
            verify(toggleCityFavourite).invoke(city)
        }
    }
}