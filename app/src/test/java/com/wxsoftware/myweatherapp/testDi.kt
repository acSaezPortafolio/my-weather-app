package com.wxsoftware.myweatherapp

import com.wxsoftware.data.datasource.LocalDataSource
import com.wxsoftware.data.datasource.LocationDataSource
import com.wxsoftware.data.datasource.RemoteDataSource
import com.wxsoftware.data.repository.PermissionChecker
import com.wxsoftware.domain.ForecastRadiusApiRequest
import com.wxsoftware.domain.forecast_radius.City
import com.wxsoftware.myweatherapp.data.database.CityDatabase
import com.wxsoftware.myweatherapp.ui.dataModule
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module

fun initMockedDi(vararg modules: Module) {
    startKoin {
        modules(listOf(mockedAppModule, dataModule) + modules)
    }
}

private val mockedAppModule = module {
    single(named("apiKey")) { "1234" }
    single { CityDatabase.build(get()) }
    single<LocalDataSource> { FakeLocalDataSource() }
    single<RemoteDataSource> { FakeRemoteDataSource() }
    single<LocationDataSource> { FakeLocationDataSource() }
    single<PermissionChecker> { FakePermissionChecker() }
    single { Dispatchers.Unconfined }
    single(named("accessCoarseLocationPermission")) { "accessCoarseLocationPermission" }
    single(named("fineLocationPermission")) { "fineLocationPermission" }
    single(named("internetPermission")){ "internetPermission" }
}

val defaultFakeCities = listOf(
    cityMocked.copy(1),
    cityMocked.copy(2),
    cityMocked.copy(3),
    cityMocked.copy(4)
)

val defaultForecastRadiusApiRequest = ForecastRadiusApiRequest(
    cod = "abcd",
    count = 1,
    list = defaultFakeCities,
    message = "fakemessage"
)


class FakeLocalDataSource: LocalDataSource {

    var cities: List<City> = emptyList()

    override suspend fun isEmpty(): Boolean = cities.isEmpty()

    override suspend fun saveCities(cities: List<City>) {
        this.cities = cities
    }

    override suspend fun getCitiesAround(): List<City> = cities

    override suspend fun findCityById(cityId: Int): City = cities.first { it.id == cityId }

    override suspend fun nukeData() = cities.toMutableList().clear()

    override suspend fun update(city: City) {
        cities = cities.filter { it.id !=  city.id } + city
    }
}

class FakeRemoteDataSource: RemoteDataSource {

    var forecastRadiusApiRequest = defaultForecastRadiusApiRequest

    override suspend fun getForecastRadiusApiRequest(
        apiKey: String,
        lat: Double,
        long: Double
    ): ForecastRadiusApiRequest = forecastRadiusApiRequest
}

class FakeLocationDataSource: LocationDataSource {

    var lastLocation = Pair(0.0, 0.0)
    var gpsEnabled = true

    override suspend fun isGpsEnabled(): Boolean = gpsEnabled

    override suspend fun lastLocationSuspended(): Pair<Double, Double>? = lastLocation
}

class FakePermissionChecker: PermissionChecker {

    var permissionChecker = true

    override suspend fun request(permissions: List<String>): Boolean = permissionChecker

}