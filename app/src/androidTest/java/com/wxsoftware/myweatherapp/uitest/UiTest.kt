package com.wxsoftware.myweatherapp.uitest

import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import com.jakewharton.espresso.OkHttp3IdlingResource
import com.wxsoftware.myweatherapp.R
import com.wxsoftware.myweatherapp.service.apiweather.OpenWeather
import com.wxsoftware.myweatherapp.ui.main.MainActivity
import com.wxsoftware.myweatherapp.utils.readJsonFile
import okhttp3.mockwebserver.MockResponse
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.test.KoinTest
import org.koin.test.get


class UiTest: KoinTest {

    @get:Rule
    val mockWebServerRule = WebServiceMockRules()

    @get:Rule
    val activityTestRule = ActivityTestRule(MainActivity::class.java, false, false)

    @get:Rule
    val grantPermissionRule: GrantPermissionRule =
        GrantPermissionRule.grant(
            "android.permission.ACCESS_COARSE_LOCATION"
        )

    @Before
    fun setUp() {
        mockWebServerRule.server.enqueue(
            MockResponse().setBody(readJsonFile(
                ApplicationProvider.getApplicationContext(),
                "citiesaround.json"
            ))
        )

        val resource = OkHttp3IdlingResource.create("OkHttp", get<OpenWeather>().okHttpClient)
        IdlingRegistry.getInstance().register(resource)
    }

    @Test
    fun clickItemNavigateToDetail() {
        activityTestRule.launchActivity(null)

        try {
            Thread.sleep(2000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        onView(withId(R.id.citiesList)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                1,
                click()
            )
        )

        onView(withId(R.id.city)).check(
            matches(withText("Los Altos Hills"))
        )
    }
}