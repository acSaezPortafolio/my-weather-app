package com.wxsofware.data.repository

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import com.wxsoftware.data.datasource.LocationDataSource
import com.wxsoftware.data.repository.LocationRepository
import com.wxsoftware.data.repository.PermissionChecker
import org.junit.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LocationRepositoryTest {

    @Mock
    lateinit var locationDataSource: LocationDataSource

    @Mock
    lateinit var permissionChecker: PermissionChecker

    private val permission = "android.permission.ACCESS_FINE_LOCATION"

    private lateinit var locationRepository: LocationRepository

    @Before
    fun setUp() {
        locationRepository =
            LocationRepository(locationDataSource, permissionChecker, permission)
    }

    @Test
    fun `return null if permission is rejected`() {
        runBlocking {
            whenever(permissionChecker.request(listOf(permission))).thenReturn(false)

            val lastLocation = locationRepository.getLastLocation()

            assertNull(lastLocation)
        }
    }

    @Test
    fun `return null if gps is not enabled`() {
        runBlocking {
            whenever(permissionChecker.request(any())).thenReturn(true)
            whenever(locationDataSource.isGpsEnabled()).thenReturn(false)

            val lastLocation = locationRepository.getLastLocation()

            assertNull(lastLocation)
        }
    }


    @Test
    fun `return location if permission is granted and gps is enabled`() {
        runBlocking {
            val location = Pair(0.0, 0.0)

            whenever(permissionChecker.request(listOf(permission))).thenReturn(true)
            whenever(locationDataSource.isGpsEnabled()).thenReturn(true)
            whenever(locationDataSource.lastLocationSuspended()).thenReturn(location)

            val lastLocation: Pair<Double, Double>? = locationRepository.getLastLocation()

            assertEquals(lastLocation, location)
        }
    }

}