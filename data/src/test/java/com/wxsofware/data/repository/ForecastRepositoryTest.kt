package com.wxsofware.data.repository

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.wxsoftware.data.datasource.LocalDataSource
import com.wxsoftware.data.datasource.RemoteDataSource
import com.wxsoftware.data.repository.ForecastRepository
import com.wxsoftware.data.repository.LocationRepository
import com.wxsoftware.domain.ForecastRadiusApiRequest
import com.wxsoftware.domain.forecast_radius.City
import com.wxsoftware.domain.forecast_radius.Sys
import org.junit.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ForecastRepositoryTest {

    @Mock
    lateinit var localDataSource: LocalDataSource

    @Mock
    lateinit var remoteDataSource: RemoteDataSource

    @Mock
    lateinit var locationRepository: LocationRepository

    private lateinit var forecastRepository: ForecastRepository

    private val apiKey = "abcd1efg"


    @Before
    fun setUp() {
        forecastRepository = ForecastRepository(
            localDataSource, remoteDataSource, apiKey, locationRepository
        )
    }


    @Test
    fun `getCitiesAround gets from localDataSource first`() {
        runBlocking {
            val localCities = listOf(cityMocked.copy(1))

            whenever(localDataSource.isEmpty()).thenReturn(false)
            whenever(localDataSource.getCitiesAround()).thenReturn(localCities)

            val cityList = forecastRepository.getCitiesAround()

            assertEquals(localCities, cityList)
        }
    }


    @Test
    fun `getCitiesAround saves cities in local`() {
        runBlocking {
            val remoteCities = listOf(cityMocked.copy(2))

            val remoteForecastRadiusApiRequest =
                forecastRadiusApiRequest.copy("a", 1, remoteCities, "d")


            whenever(localDataSource.isEmpty()).thenReturn(true)
            whenever(locationRepository.getLastLocation()).thenReturn(Pair(0.0, 0.0))
            whenever(remoteDataSource.getForecastRadiusApiRequest(any(), any(), any())).thenReturn(remoteForecastRadiusApiRequest)

            forecastRepository.getCitiesAround()

            verify(localDataSource).saveCities(remoteCities)
        }
    }

    @Test
    fun `findCityById calls localLocalDataSource`() {
        runBlocking {
            val city = cityMocked.copy(3)

            whenever(localDataSource.findCityById(3)).thenReturn(city)

            val cityTest = forecastRepository.findCityById(3)

            assertEquals(city, cityTest)
        }
    }

    @Test
    fun `update updates local data source`() {
        runBlocking {
            val city = cityMocked.copy(4)

            forecastRepository.update(city)

            verify(localDataSource).update(city)
        }
    }


    private val cityMocked = City(
        name = "cityMocked",
        sys = Sys("Spain")
    )

    private val forecastRadiusApiRequest = ForecastRadiusApiRequest(
        "",
        1,
        listOf(),
        ""
    )
}