package com.wxsoftware.data.repository

import com.wxsoftware.data.datasource.LocationDataSource


class LocationRepository(
    private val locationDataSource: LocationDataSource,
    private val permissionChecker: PermissionChecker,
    private vararg val permissions: String
    ) {

    suspend fun getLastLocation(): Pair<Double, Double>? {
        val success = permissionChecker.request(permissions.asList())
        return if (success && locationDataSource.isGpsEnabled()) locationDataSource.lastLocationSuspended() else null
    }
}

interface PermissionChecker {
    suspend fun request(permissions: List<String>): Boolean
}