package com.wxsoftware.data.repository

import com.wxsoftware.data.datasource.LocalDataSource
import com.wxsoftware.data.datasource.RemoteDataSource
import com.wxsoftware.domain.forecast_radius.City

class ForecastRepository(
    private val localDataSource: LocalDataSource,
    private val remoteDataSource: RemoteDataSource,
    private val apiKey: String,
    private val locationRepository: LocationRepository
) {

    suspend fun getCitiesAround(): List<City> {
        if (localDataSource.isEmpty()) {
            val location = locationRepository.getLastLocation()
            location?.let {
                val forecastRadiusApiRequest =
                    remoteDataSource.getForecastRadiusApiRequest(apiKey, location.first, location.second)
                localDataSource.saveCities(forecastRadiusApiRequest.list)
            }
        }

        return localDataSource.getCitiesAround()
    }

    suspend fun findCityById(cityId: Int): City = localDataSource.findCityById(cityId)

    suspend fun update(city: City) = localDataSource.update(city)
}