package com.wxsoftware.data.datasource

import com.wxsoftware.domain.ForecastRadiusApiRequest

interface RemoteDataSource {
    suspend fun getForecastRadiusApiRequest(
        apiKey: String,
        lat: Double,
        long: Double
    ): ForecastRadiusApiRequest
}