package com.wxsoftware.data.datasource

interface LocationDataSource {

    suspend fun isGpsEnabled(): Boolean
    suspend fun lastLocationSuspended(): Pair<Double, Double>?
}