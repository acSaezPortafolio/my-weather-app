package com.wxsoftware.data.datasource

import com.wxsoftware.domain.forecast_radius.City

interface LocalDataSource {
    suspend fun isEmpty(): Boolean
    suspend fun saveCities(cities: List<City>)
    suspend fun getCitiesAround(): List<City>
    suspend fun findCityById(cityId: Int): City
    suspend fun nukeData()
    suspend fun update(city: City)
}