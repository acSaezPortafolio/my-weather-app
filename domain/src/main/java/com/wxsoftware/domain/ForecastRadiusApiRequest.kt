package com.wxsoftware.domain

import com.wxsoftware.domain.forecast_radius.City

data class ForecastRadiusApiRequest(
    val cod: String,
    val count: Int,
    val list: List<City>,
    val message: String
)