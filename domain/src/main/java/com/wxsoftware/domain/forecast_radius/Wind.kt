package com.wxsoftware.domain.forecast_radius


data class Wind(
    val deg: Int,
    val gust: Int,
    val speed: Double
)