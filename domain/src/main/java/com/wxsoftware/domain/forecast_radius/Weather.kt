package com.wxsoftware.domain.forecast_radius

data class Weather(
    var description: String = "",
    var icon: String = "",
    var id: Int? = 0,
    var main: String = "",
    var cityId: Int = 0
)