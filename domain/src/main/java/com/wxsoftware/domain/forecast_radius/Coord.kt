package com.wxsoftware.domain.forecast_radius

data class Coord(
    val lat: Double,
    val lon: Double
)