package com.wxsoftware.domain.forecast_radius

data class City(
    var id: Int = 0,
    var clouds: Clouds = Clouds(0),
    var coord: Coord = Coord(0.0, 0.0),
    var dt: Int = 0,
    var main: Main = Main(0,0,0.0,0.0,0.0),
    var name: String = "",
    var sys: Sys = Sys(""),
    var weather: List<Weather> = arrayListOf(),
    var wind: Wind = Wind(0,0,0.0),
    var favourite: Boolean = false
)