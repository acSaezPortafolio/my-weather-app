package com.wxsoftware.usecases

import com.nhaarman.mockitokotlin2.whenever
import com.wxsoftware.data.repository.ForecastRepository
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.junit.Assert.assertEquals

@RunWith(MockitoJUnitRunner::class)
class GetAroundLocationTest {

    @Mock
    lateinit var forecastRepository: ForecastRepository

    lateinit var getAroundLocation: GetAroundLocation

    @Before
    fun setUp() {
        getAroundLocation = GetAroundLocation(forecastRepository)
    }

    @Test
    fun `getAroundLocation calls forecastRepository`() {
        runBlocking {
            val cities = listOf(cityMocked.copy(2))

            whenever(forecastRepository.getCitiesAround()).thenReturn(cities)

            val response = getAroundLocation.invoke()

            assertEquals(response, cities)
        }
    }
}