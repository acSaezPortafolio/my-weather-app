package com.wxsoftware.usecases

import com.nhaarman.mockitokotlin2.verify
import com.wxsoftware.data.repository.ForecastRepository
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.junit.Assert.*

@RunWith(MockitoJUnitRunner::class)
class ToggleCityFavouriteTest {

    @Mock
    lateinit var forecastRepository: ForecastRepository

    lateinit var toggleCityFavourite: ToggleCityFavourite

    @Before
    fun setUp() {
        toggleCityFavourite = ToggleCityFavourite(forecastRepository)
    }

    @Test
    fun `invoke calls forecastRepository`() {
        runBlocking {
            val city = cityMocked.copy(1)

            val result = toggleCityFavourite.invoke(city)

            verify(forecastRepository).update(result)
        }
    }

    @Test
    fun `favourite city becomes unfavourite`() {
        runBlocking {
            val city = cityMocked.copy(favourite = true)

            val result = toggleCityFavourite.invoke(city)

            assertFalse(result.favourite)
        }
    }

    @Test
    fun `unfavourite city becomes favourite`() {
        runBlocking {
            val city = cityMocked.copy(favourite = false)

            val result = toggleCityFavourite.invoke(city)

            assertTrue(result.favourite)
        }
    }
}