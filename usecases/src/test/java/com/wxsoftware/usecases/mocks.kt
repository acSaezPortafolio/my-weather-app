package com.wxsoftware.usecases

import com.wxsoftware.domain.ForecastRadiusApiRequest
import com.wxsoftware.domain.forecast_radius.City
import com.wxsoftware.domain.forecast_radius.Sys


    internal val cityMocked = City(
        name = "cityMocked",
        sys = Sys("Spain")
    )

    internal val forecastRadiusApiRequest = ForecastRadiusApiRequest(
        "",
        1,
        listOf(),
        ""
    )
