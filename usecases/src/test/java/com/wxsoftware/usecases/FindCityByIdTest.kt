package com.wxsoftware.usecases

import com.nhaarman.mockitokotlin2.whenever
import com.wxsoftware.data.repository.ForecastRepository
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.junit.Assert.assertEquals

@RunWith(MockitoJUnitRunner::class)
class FindCityByIdTest {

    @Mock
    lateinit var forecastRepository: ForecastRepository

    lateinit var findCityById: FindCityById

    @Before
    fun setUp() {
        findCityById = FindCityById(forecastRepository)
    }

    @Test
    fun `invoke calls forecastRepository`() {
        runBlocking {
            val city = cityMocked.copy(id = 5)

            whenever(forecastRepository.findCityById(5)).thenReturn(city)

            val response = findCityById.invoke(5)

            assertEquals(response, city)
        }
    }
}