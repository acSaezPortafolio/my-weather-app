package com.wxsoftware.usecases

import com.wxsoftware.data.repository.ForecastRepository

class FindCityById(private val forecastRepository: ForecastRepository) {

    suspend fun invoke(cityId: Int) = forecastRepository.findCityById(cityId)
}