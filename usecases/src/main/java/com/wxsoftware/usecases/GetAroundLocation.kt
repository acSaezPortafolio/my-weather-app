package com.wxsoftware.usecases


import com.wxsoftware.data.repository.ForecastRepository
import com.wxsoftware.domain.forecast_radius.City

class GetAroundLocation(private val forecastRepository: ForecastRepository) {

    suspend fun invoke(): List<City> = forecastRepository.getCitiesAround()

}