package com.wxsoftware.usecases

import com.wxsoftware.data.repository.ForecastRepository
import com.wxsoftware.domain.forecast_radius.City

class ToggleCityFavourite(private val forecastRepository: ForecastRepository) {

    suspend fun invoke(city: City): City = with (city) {
        this.copy(favourite = !this.favourite).apply { forecastRepository.update(this) }
    }
}